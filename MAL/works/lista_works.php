<div class="container">
  <h2>Obras</h2>
  <a class="btn btn-info" href="works.php?acao=novo&userid=<?php echo $_GET['id']?>">Novo</a>
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Nome</th>
          <th>Tipo de obra</th>
          <th>Data de lançamento</th>
          <th>Status da obra</th>
          <th>Nome da Produtora</th>
          <th>Nome do Autor</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['work_id']; ?></td>
            <td><?php echo $linha['name']; ?></td>
            <td><?php echo $linha['work_type']; ?></td>
            <td><?php echo $linha['release_date']; ?></td>
            <td><?php echo $linha['work_status']; ?></td>
            <td><?php echo $linha['producer_name']; ?></td>
            <td><?php echo $linha['author_name']; ?></td>
            <td>
                <a class="btn btn btn-success btn-sm" href="works.php?acao=adicionar&workid=<?php echo $linha['work_id'];?>&userid=<?php echo $_GET['id'];?>">Adicionar</a>
                <a class="btn btn-warning btn-sm" href="works.php?acao=buscar&id=<?php echo $linha['work_id']; ?>&userid=<?php echo $_GET['id'];?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="works.php?acao=excluir&id=<?php echo $linha['work_id']; ?>&userid=<?php echo $_GET['id'];?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
