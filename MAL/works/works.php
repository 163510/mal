<?php

    require_once '../config/conexao.php';


    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){

       $sql   = "SELECT w.work_id AS work_id, w.name AS name, w.work_type AS work_type, w.release_date AS release_date,
       w.work_status AS work_status, a.name AS author_name, p.producer_name AS producer_name
       FROM work w INNER JOIN author a ON w.author_id = a.author_id
       INNER JOIN producer p ON w.producer_id = p.producer_id";

       $query = $con->query($sql);
       $registros = $query->fetchAll();

       require_once '../template/cabecalho.php';
       require_once 'lista_works.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){

      //Authors sql
      $authorSql = "SELECT author_id, name FROM author";
      $authorQuery =  $con->query($authorSql);
      $lista_author = $authorQuery->fetchAll();

      //Producers sql
      $prodSql = "SELECT producer_id, producer_name FROM producer";
      $producerQuery = $con->query($prodSql);
      $lista_producers = $producerQuery->fetchAll();

      require_once '../template/cabecalho.php';
      require_once 'form_works.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;

        // var_dump($registro);
        $sql = "INSERT INTO work(name, work_type, release_date, work_status, author_id, producer_id) VALUES(:name, :work_type, :release_date, :work_status, :author_id, :producer_id)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./works.php?id='.$_GET['userid']);
        }else{
            echo "Erro ao tentar inserir o registro";
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){

        $id = $_GET['id'];

        // Remove da tabela associativa
        $sql1   = "DELETE FROM consumed_works WHERE work_id = :id";
        $query1 = $con->prepare($sql1);
        $query1->bindParam(':id', $id);
        $query1->execute();

        // remove da tabela de obras
        $sql   = "DELETE FROM work WHERE work_id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();

        if($result){
            header('Location: ./works.php?id='.$_GET['userid']);
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "buscar"){
        //works
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM work WHERE work_id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        //Authors sql
        $authorSql = "SELECT author_id, name FROM author";
        $authorQuery =  $con->query($authorSql);
        $lista_author = $authorQuery->fetchAll();

        //Producers sql
        $prodSql = "SELECT producer_id, producer_name FROM producer";
        $producerQuery = $con->query($prodSql);
        $lista_producers = $producerQuery->fetchAll();

        // var_dump($registro); exit;

        //id do usuario
        $userId = $_GET['userid'];

        require_once '../template/cabecalho.php';
        require_once 'form_works.php';
        require_once '../template/rodape.php';

    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE work SET name = :name, work_type = :work_type, work_status = :work_status, author_id = :author_id, producer_id = :producer_id WHERE work_id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':name', $_POST['name']);
        $query->bindParam(':work_type', $_POST['work_type']);
        $query->bindParam(':work_status', $_POST['work_status']);
        $query->bindParam(':author_id', $_POST['author_id']);
        $query->bindParam(':producer_id', $_POST['producer_id']);
        $result = $query->execute();

        // $registro = array('id'  =>$_GET['id'],
        //                   'nome'=>$_POST['nome']);
        // $result = $query->execute($registro);

        if($result){
            header('Location: ./works.php?id='.$_GET['userid']);
        }else{
            echo "Erro ao tentar atualizar os dados";
        }
    }

    /**
    * Adicionar Anime a lista pessoal
    **/
    else if($acao == "adicionar") {

       $sql = "INSERT INTO consumed_works (work_id, user_id) VALUES (:work_id, :user_id)";

       $query = $con->prepare($sql);

       $query->bindParam(":work_id", $_GET['workid']);
       $query->bindParam(":user_id", $_GET['userid']);

       $result = $query->execute();

       if($result){
           header('Location: ./works.php?id='.$_GET['userid']);
       }else{
           echo "Erro ao tentar inserir anime a sua lista";
       }

    }


 ?>
