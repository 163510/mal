<?php
    if(isset($registro)) $acao = "works.php?acao=atualizar&id=".$registro['work_id']."&userid=".$userId;
    else $acao = "works.php?acao=gravar&userid=".$_GET['userid'];
 ?>
 <div class="container">
   <h2>Obras</h2>
   <form class="" action="<?php echo $acao; ?>" method="post">
     <div class="from-group">
       <label for="name">Nome</label>
       <input id="name" class="form-control" type="text" name="name"
         value="<?php if(isset($registro)) echo $registro['name']; ?>" required>
     </div>
     <div class="from-group">
       <label for="work_type">Tipo da obra</label>
       <input id="work_type" class="form-control" type="text" name="work_type"
         value="<?php if(isset($registro)) echo $registro['work_type']; ?>" required>
     </div>
     <div class="from-group">
       <label for="release_date">data de lançamento</label>
       <input id="release_date" class="form-control" type="text" name="release_date"
         value="<?php if(isset($registro)) echo $registro['release_date']; ?>" required>
     </div>
     <div class="from-group">
       <label for="work_status">status da obra</label>
       <input id="work_status" class="form-control" type="text" name="work_status"
         value="<?php if(isset($registro)) echo $registro['work_status']; ?>" required>
     </div>
     <div class="from-group">
       <label for="author_id">Autor</label>
       <select class="form-control" name="author_id" required>
         <option value="">Escolha um item na lista</option>
         <?php foreach ($lista_author as $item): ?>
           <option value="<?php echo $item['author_id']; ?>"
             <?php if(isset($registro) && $registro['author_id']==$item['author_id']) echo "selected";?>>
             <?php echo $item['name']; ?>
           </option>
         <?php endforeach; ?>
       </select>
     </div>
     <div class="from-group">
       <label for="producer_id">Produtora</label>
       <select class="form-control" name="producer_id" required>
         <option value="">Escolha um item na lista</option>
         <?php foreach ($lista_producers as $item): ?>
           <option value="<?php echo $item['producer_id']; ?>"
             <?php if(isset($registro) && $registro['producer_id']==$item['producer_id']) echo "selected";?>>
             <?php echo $item['producer_name']; ?>
           </option>
         <?php endforeach; ?>
       </select>
     </div>
     <br>
     <button class="btn btn-info" type="submit">Enviar</button>
   </form>
 </div>
