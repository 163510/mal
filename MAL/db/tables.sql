create table author (
author_id 	BIGINT NOT NULL auto_increment,
name VARCHAR(255) NOT NULL,
nation VARCHAR(2) NOT NULL,
country VARCHAR(255) NOT NULL,
birth_date VARCHAR(255) NOT NULL,
primary key(author_id)
);

create table work (
work_id BIGINT NOT NULL auto_increment,
name VARCHAR(255) NOT NULL,
work_type VARCHAR(255) NOT NULL,
realease_date VARCHAR(255),
work_status VARCHAR(255) NOT NULL,
producer_id BIGINT NOT NULL,
author_id BIGINT NOT NULL,
primary key(work_id),
FOREIGN KEY(author_id) references author(author_id),
FOREIGN KEY(producer_id) references producer(producer_id)
);

create table user (
user_id BIGINT NOT NULL auto_increment,
profile_name VARCHAR(255) NOT NULL,
login VARCHAR(255) UNIQUE NOT NULL,
password VARCHAR(255) NOT NULL,
pofile_status VARCHAR(255) DEFAULT 'user' NOT NULL,
primary key(user_id)
);

create table consumed_works (
user_id BIGINT NOT NULL,
work_id BIGINT NOT NULL,
primary key(user_id, work_id)
);


create table producer (
producer_id BIGINT NOT NULL auto_increment,
producer_name VARCHAR(255) NOT NULL,
producer_country VARCHAR(255) NOT NULL,
producer_nation VARCHAR(2) NOT NULL,
primary key(producer_id)
);
