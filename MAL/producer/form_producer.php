<?php
    if(isset($registro)) $acao = "producer.php?acao=atualizar&id=".$registro['producer_id'];
    else $acao = "producer.php?acao=gravar";
 ?>
 <div class="container">
   <h2>Produtora</h2>
   <form class="" action="<?php echo $acao; ?>" method="post">
     <div class="from-group">
       <label for="producer_name">Nome</label>
       <input id="producer_name" class="form-control" type="text" name="producer_name"
         value="<?php if(isset($registro)) echo $registro['producer_name']; ?>" required>
     </div>

     <div class="from-group">
       <label for="producer_country">País</label>
       <input id="producer_country" class="form-control" type="text" name="producer_country"
         value="<?php if(isset($registro)) echo $registro['producer_country']; ?>" required>
     </div>

     <div class="from-group">
       <label for="producer_nation">Nacionalidade</label>
       <input id="producer_nation" class="form-control" type="text" name="producer_nation" size="2"
         value="<?php if(isset($registro)) echo $registro['producer_nation']; ?>" required>
     </div>
     <br>
     <button class="btn btn-info" type="submit">Enviar</button>
   </form>
 </div>
