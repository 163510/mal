<div class="container">
  <h2>Produtoras</h2>
  <a class="btn btn-info" href="producer.php?acao=novo">Novo</a>
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Nome</th>
          <th>Nacionalidade</th>
          <th>País</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['producer_id']; ?></td>
            <td><?php echo $linha['producer_name']; ?></td>
            <td><?php echo $linha['producer_nation']; ?></td>
            <td><?php echo $linha['producer_country']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="producer.php?acao=buscar&id=<?php echo $linha['producer_id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="producer.php?acao=excluir&id=<?php echo $linha['producer_id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
