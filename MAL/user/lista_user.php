<div class="container">
  <h2>Obras</h2>
  <a class="btn btn-info" href="user.php?acao=novo">Novo</a>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Nome</th>
          <th>login</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['user_id']; ?></td>
            <td><?php echo $linha['profile_name']; ?></td>
            <td><?php echo $linha['login']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="user.php?acao=buscarProfile&id=<?php echo $linha['user_id']; ?>">Atualziar senha</a>
                <a class="btn btn-warning btn-sm" href="user.php?acao=buscar&id=<?php echo $linha['user_id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="user.php?acao=excluir&id=<?php echo $linha['user_id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
</div>
