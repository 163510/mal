<div class="container">

  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <h2><?php echo $_SESSION['logado']['name'] ?> Anime list</h2>
    <table class="table table-hover table-stripped">
      <thead>
          <th>Nome da Obra</th>
          <th>Tipo</th>
          <th>Data de lançamento</th>
          <th>Nome do Autor</th>
          <th>Nome da Produtora</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['work_name']; ?></td>
            <td><?php echo $linha['work_type']; ?></td>
            <td><?php echo $linha['release_date']; ?></td>
            <td><?php echo $linha['author_name']; ?></td>
            <td><?php echo $linha['producer_name']; ?></td>
            <td>
                <a class="btn btn-danger btn-sm" href="user.php?acao=remover&workid=<?php echo $linha['work_id']; ?>&profileid=<?php echo $linha['user_id']; ?>">Remover</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
