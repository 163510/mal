<?php


    require_once '../config/conexao.php';

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql   = "SELECT * FROM user WHERE user_id = :user_id";
       $query = $con->prepare($sql);

       $query->bindParam(':user_id', $_GET['id']);

       $query->execute();
       $registros = $query->fetchAll();

       require_once '../template/cabecalho.php';
       require_once 'lista_user.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
      require_once '../template/login_cabecalho.php';
      require_once 'form_users.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){

        // var_dump($registro);
        $sql = "INSERT INTO user(profile_name, login, password) VALUES(:profile_name, :login, :password)";
        $query = $con->prepare($sql);

        $query->bindParam('profile_name', $_POST['profile_name']);
        $query->bindParam('login', $_POST['login']);

        $password = md5($_POST['password']);
        $query->bindParam('password', $password);
        $result = $query->execute();
        if($result){
            $loginSql = "SELECT * FROM user WHERE login = :login AND password = :senha";
            $loginQuery = $con->prepare($loginSql);

            $loginQuery->bindParam('login', $_POST['login']);
            $loginQuery->bindParam('senha', $password);
            $loginQuery->execute();
            if($loginQuery->rowCount()==1){
                session_start();
                $usuario = $loginQuery->fetch();
                // var_dump($usuario);
                $_SESSION['logado'] = array("name"=>$usuario['profile_name'], 'id'=>$usuario['user_id']);
                header('Location: ../index.php');
            }else{
                header('Location: ../login.php');
            }
        }else{
            echo "Erro ao realizar o cadastro, por favor tente com um login diferente";
            header('Location: ../user/user.php?acao=novo');
        }
    }

    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];

        //remove da tabela associativa
        $sql1   = "DELETE FROM consumed_works WHERE user_id = :id";
        $query1 = $con->prepare($sql1);
        $query1->bindParam(':id', $id);
        $query1->execute();

        // remove usuario da base
        $sql   = "DELETE FROM user WHERE user_id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ../login.php?acao=sair');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "buscar"){
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM user WHERE user_id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;

        require_once '../template/cabecalho.php';
        require_once 'form_users_update.php';
        require_once '../template/rodape.php';

    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE user SET profile_name = :profile_name, login = :login WHERE user_id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':profile_name', $_POST['profile_name']);
        $query->bindParam(':login', $_POST['login']);
        $result = $query->execute();

        // $registro = array('id'  =>$_GET['id'],
        //                   'nome'=>$_POST['nome']);
        // $result = $query->execute($registro);

        if($result){
            header('Location: ./user.php?id=' . $_GET['id']);
        }else{
            echo "Erro ao tentar atualizar os dados";
        }
    }
    else if($acao == "buscarProfile"){
        $sql = "SELECT * FROM user WHERE user_id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $_GET['id']);

        $query->execute();
        $registro = $query->fetch();

        require_once '../template/cabecalho.php';
        require_once 'form_users_update_password.php';
        require_once '../template/rodape.php';

    }
    /**
    *Ação de atualizar senha
    **/
    else if($acao == "attSenha"){

      $sql = "SELECT * FROM user WHERE user_id = :id";
      $query = $con->prepare($sql);

      $query->bindParam(':id', $_GET['id']);

      $query->execute();
      $registro = $query->fetch();

        if(md5($_POST['oldPassowrd']) == $registro['password']){

          $uptQuery = "UPDATE user SET password = :password WHERE user_id = :id";
          $uptQuery = $con->prepare($uptQuery);

          $newPassword = md5($_POST['newPassword']);

          $uptQuery->bindParam(":id", $_GET['id']);
          $uptQuery->bindParam(":password", $newPassword);

          $result = $uptQuery->execute();

          if($result){
              header('Location: ./user.php?id=' . $_GET['id']);
          }else{
              echo "Erro ao tentar trocar de senha";
          }



        }else{
          echo "senha informada não condiz com a antiga";
        }
    }
    /**
    * Ação de lsitar meus animes
    **/
    else if($acao == "myanimes"){

      $sql = "SELECT u.user_id AS user_id,
                     w.work_id AS work_id,
                     w.name AS work_name,
                     w.work_type AS work_type,
                     w.release_date AS release_date,
                     a.name AS author_name,
                     p.producer_name AS producer_name
        FROM consumed_works c
        INNER JOIN work w ON c.work_id = w.work_id
        INNER JOIN author a ON w.author_id = a.author_id
        INNER JOIN producer p ON w.producer_id = p.producer_id
        INNER JOIN user u ON u.user_id = c.user_id
        WHERE c.user_id = :user_id";

        $query = $con->prepare($sql);

        $query->bindParam("user_id", $_GET['id']);

        $query->execute();

        $registros = $query->fetchAll();

        require_once '../template/cabecalho.php';
        require_once 'user_anime_list.php';
        require_once '../template/rodape.php';

    }

    /**
    * Remvoer um anime da lista
    **/
    else if($acao == "remover"){

        $sql = "DELETE FROM consumed_works WHERE work_id = :work_id AND user_id = :user_id";

        $query = $con->prepare($sql);

        $query->bindParam(":work_id", $_GET['workid']);
        $query->bindParam(":user_id", $_GET['profileid']);

        $result = $query->execute();

        if($result){
            header('Location: ./user.php?acao=myanimes&id=' . $_GET['profileid']);
        }else{
            echo "Erro ao tentar excluir anime da lista";
        }

    }

 ?>
