<?php
    $acao = "user.php?acao=attSenha&id=".$registro['user_id'];
 ?>
 <div class="container">
   <h2>Cadastre-se</h2>
   <form class="" action="<?php echo $acao; ?>" method="post">
     <div class="from-group">
       <label for="oldPassowrd">Senha antiga</label>
       <input id="oldPassowrd" class="form-control" type="password" name="oldPassowrd" required>
     </div>

     <div class="from-group">
       <label for="newPassword">Nova senha</label>
       <input id="newPassword" class="form-control" type="password" name="newPassword" required>
     </div>

     <br>
     <button class="btn btn-info" type="submit">Atualizar</button>
   </form>
 </div>
