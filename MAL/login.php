<?php
    session_start(); //DEVE SER A PRIMEIRA LINHA

    //Finaliza a sessão logado da Aplicação
    if(isset($_GET['acao']) && $_GET['acao']=="sair"){
        unset($_SESSION['logado']);
    }

    if(isset($_POST)){
        require_once './config/conexao.php';
        $sql   = "SELECT * FROM user WHERE login = :login AND password = :senha";
        $query = $con->prepare($sql);
        $query->bindParam('login', $_POST['login']);

        //Colocar a senha como md5 utilizando a função md5()
        if(isset($_POST['login']) && isset($_POST['senha'])){
          $senha = md5($_POST['senha']);

          $query->bindParam('senha', $senha);
          $query->execute();
          if($query->rowCount()==1){
              $usuario = $query->fetch();
              $_SESSION['logado'] = array("name"=>$usuario['profile_name'], 'id'=>$usuario['user_id']);
              header('Location: ./index.php');
          }else{
              $msg = "Usuário ou senha não conferem";
          }
        }
    }
 ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Login My Anime List</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sign-in/">


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="./template/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <div class="container">
    <form action="login.php" method="post" class="form-signin">
      <?php if (isset($msg)) { ?>
        <div class="alert alert-danger" role="alert">
          <?= $msg; ?>
        </div>
      <?php } ?>
      <img class="mb-4" src="https://store-images.s-microsoft.com/image/apps.63654.9007199266506523.6eac6b6a-8bb6-459f-b5bb-28383cc469bc.1da183a6-e5d8-4b93-8abd-3e2de0139603?mode=scale&q=90&h=200&w=200&background=%230078D7">
      <h1 class="h3 mb-3 font-weight-normal">Informe seus dados</h1>
      <div class="">
      <label for="inputLogin" class="sr-only">login</label>
      <input name="login" type="email" id="inputLogin" class="form-control" placeholder="login" required autofocus>
      <label for="inputPassword" class="sr-only">Senha</label>
      <input name="senha" type="password" id="inputPassword" class="form-control" placeholder="senha" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      <a class="btn btn-lg btn-primary btn-block" href="user/user.php?acao=novo">Cadastrar-se</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>
    </div>
  </body>
</html>
