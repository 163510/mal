<?php
    if(isset($registro)) $acao = "author.php?acao=atualizar&id=".$registro['author_id'];
    else $acao = "author.php?acao=gravar";
 ?>
 <div class="container">
   <h2>Autores</h2>
   <form class="" action="<?php echo $acao; ?>" method="post">
     <div class="from-group">
       <label for="name">Nome</label>
       <input id="name" class="form-control" type="text" name="name"
         value="<?php if(isset($registro)) echo $registro['name']; ?>" required>
     </div>
     <div class="from-group">
       <label for="nation">Nacionalidade</label>
       <input id="nation" class="form-control" type="text" name="nation" size="2"
         value="<?php if(isset($registro)) echo $registro['nation']; ?>" required>
     </div>
     <div class="from-group">
       <label for="country">País</label>
       <input id="country" class="form-control" type="text" name="country"
         value="<?php if(isset($registro)) echo $registro['country']; ?>" required>
     </div>
     <div class="from-group">
       <label for="birth_date">Data de Nascimento</label>
       <input id="birth_date" class="form-control" type="text" name="birth_date"
         value="<?php if(isset($registro)) echo $registro['birth_date']; ?>" required>
     </div>
     <br>
     <button class="btn btn-info" type="submit">Enviar</button>
   </form>
 </div>
