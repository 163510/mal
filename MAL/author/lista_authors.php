<div class="container">
  <h2>Autores</h2>
  <a class="btn btn-info" href="author.php?acao=novo">Novo</a>
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Nome</th>
          <th>Nacionalidade</th>
          <th>País</th>
          <th>Data de Nascimento</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['author_id']; ?></td>
            <td><?php echo $linha['name']; ?></td>
            <td><?php echo $linha['nation']; ?></td>
            <td><?php echo $linha['country']; ?></td>
            <td><?php echo $linha['birth_date']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="author.php?acao=buscar&id=<?php echo $linha['author_id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="author.php?acao=excluir&id=<?php echo $linha['author_id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
