<?php
    require_once '../config/conexao.php';

    //genero/genero.php?acao=listar

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql   = "SELECT * FROM author";
       $query = $con->query($sql);
       $registros = $query->fetchAll();

       require_once '../template/cabecalho.php';
       require_once 'lista_authors.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
      require_once '../template/cabecalho.php';
      require_once 'form_authors.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;

        // var_dump($registro);
        $sql = "INSERT INTO author(name, nation, country, birth_date) VALUES(:name, :nation, :country, :birth_date)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./author.php');
        }else{
            echo "Erro ao tentar inserir o registro";
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];

        //remove obras daquele autor
        $sql1   = "DELETE FROM work WHERE author_id = :id";
        $query1 = $con->prepare($sql1);
        $query1->bindParam(':id', $id);
        $query1->execute();

        //remove autor
        $sql   = "DELETE FROM author WHERE author_id = :author_id";
        $query = $con->prepare($sql);

        $query->bindParam(':author_id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./author.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "buscar"){
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM author WHERE author_id = :author_id";
        $query = $con->prepare($sql);
        $query->bindParam(':author_id', $id);

        $query->execute();
        $registro = $query->fetch();

        //var_dump($registro); exit;

        require_once '../template/cabecalho.php';
        require_once 'form_authors.php';
        require_once '../template/rodape.php';

    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE author SET name = :name, nation = :nation, country = :country, birth_date = :birth_date WHERE author_id = :author_id";
        $query = $con->prepare($sql);

        $query->bindParam(':author_id', $_GET['id']);
        $query->bindParam(':name', $_POST['name']);
        $query->bindParam(':nation', $_POST['nation']);
        $query->bindParam(':country', $_POST['country']);
        $query->bindParam(':birth_date', $_POST['birth_date']);
        $result = $query->execute();

        // $registro = array('id'  =>$_GET['id'],
        //                   'nome'=>$_POST['nome']);
        // $result = $query->execute($registro);

        if($result){
            header('Location: ./author.php');
        }else{
            echo "Erro ao tentar atualizar os dados";
        }
    }

 ?>
