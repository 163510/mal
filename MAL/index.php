<?php require_once 'template/cabecalho.php'; ?>

<section class="jumbotron text-center">
  <div class="container">
    <h1 class="jumbotron-heading">Bem Vindo ao My Anime List</h1>
    <h3 class="jumbotron-heading">A sua patlaforma de gerenciamento de anime</h1>
    <p class="lead text-muted">
      Aplicação para auxiliar na tarefa de gerenciar animes, aqui você pode adicionar os animes que já viu, está vendo ou planeja ver em sua lista.
    </p>
    <p>
      <a href="<?= BASE_URL; ?>/works/works.php" class="btn btn-primary my-2">Veja as Obras cadastradas</a>
      <a href="<?= BASE_URL; ?>/user/user.php?acao=myanimes&id=<?php echo $_SESSION['logado']['id']; ?>" class="btn btn-secondary my-2">Sua lista de animes</a>
    </p>
  </div>
</section>

<?php require_once 'template/rodape.php'; ?>
